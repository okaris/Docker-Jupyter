FROM jupyter/base-notebook
MAINTAINER Jupyter Project <jupyter@googlegroups.com>

USER root
RUN apt-get update -y && \
    apt-get install vim -y

USER jovyan

# install Python + NodeJS with conda
RUN wget -q https://repo.continuum.io/miniconda/Miniconda3-4.2.12-Linux-x86_64.sh -O /tmp/miniconda.sh  && \
    echo 'd0c7c71cc5659e54ab51f2005a8d96f3 */tmp/miniconda.sh' | md5sum -c - && \
    bash /tmp/miniconda.sh -f -b -p /opt/conda && \
    /opt/conda/bin/conda update conda --yes && \
    /opt/conda/bin/conda install --yes -c conda-forge python=3.5 pandas numpy scikit-learn seaborn matplotlib pymongo scipy shapely geopy statsmodels openpyxl python-dateutil requests tensorflow psycopg2 sqlalchemy tornado jinja2 traitlets requests pip configurable-http-proxy && \
    /opt/conda/bin/pip install --upgrade pip && \
    rm /tmp/miniconda.sh
ENV PATH=/opt/conda/bin:$PATH

VOLUME ~/work:/work

EXPOSE 8888
